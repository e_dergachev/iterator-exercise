function Iterable(length) {
    this.length = length;
}
Iterable.prototype[Symbol.iterator] = function*() {
    for (let i=0; i < this.length; i++) {
        yield (Math.random().toFixed(2) * 2).toFixed(2);
    }
}

const it = new Iterable(6);
console.log([... it]);
